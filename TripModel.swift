//
//  TripModel.swift
//  itinearary
//
//  Created by Dan Li on 2018/7/6.
//  Copyright © 2018年 DanStenLee. All rights reserved.
//

import Foundation

class TripModel{
    var id: String
    var title: String
    
    init(title:String) {
        id=UUID().uuidString
        self.title=title
    }
}


