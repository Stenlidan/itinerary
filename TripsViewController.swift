//
//  TripsViewController.swift
//  itinearary
//
//  Created by Dan Li on 2018/7/8.
//  Copyright © 2018年 DanStenLee. All rights reserved.
//

import UIKit

class TripsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate=self
        tableView.dataSource=self
        
        addButton.addShadowForButton()
        
        view.backgroundColor=UIColor.white
        
        TripFunctions.readTrips(completion: {[weak self] in
            self?.tableView.reloadData()
            
        })
    }
}
extension TripsViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Data.tripModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TripsTableViewCell
        cell.setup(tripModel: Data.tripModel[indexPath.row])
        return cell
    }
    
}
