//
//  TripsTableViewCell.swift
//  itinearary
//
//  Created by Dan Li on 09.07.18.
//  Copyright © 2018 DanStenLee. All rights reserved.
//

import UIKit

class TripsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cardView.addShadowAndCorner()
        titleLabel.font=UIFont(name: Theme.mainFontName, size: 58)
        
    }
    
    func setup(tripModel:TripModel){
        titleLabel.text=tripModel.title
    }

}
