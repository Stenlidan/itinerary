//
//  ExtensionUIView.swift
//  itinearary
//
//  Created by Dan Li on 11.07.18.
//  Copyright © 2018 DanStenLee. All rights reserved.
//

import UIKit

extension UIView {

    func addShadowAndCorner(){
        
        layer.shadowOpacity=1
        layer.shadowOffset=CGSize.zero
        layer.shadowColor=UIColor.darkGray.cgColor
        layer.cornerRadius=10
    }

}
