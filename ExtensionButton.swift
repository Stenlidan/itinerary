//
//  ExtensionButton.swift
//  itinearary
//
//  Created by Dan Li on 12.07.18.
//  Copyright © 2018 DanStenLee. All rights reserved.
//

import UIKit

extension UIButton {
    func addShadowForButton(){
//        backgroundColor=Theme.tint
        layer.cornerRadius=frame.height/2
        layer.shadowOpacity=0.25
        layer.shadowRadius=5
        layer.shadowOffset=CGSize(width: 0, height: 10)

    }
}
