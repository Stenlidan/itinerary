//
//  Theme.swift
//  itinearary
//
//  Created by Dan Li on 11.07.18.
//  Copyright © 2018 DanStenLee. All rights reserved.
//

import UIKit
class Theme{
    static let mainFontName="Futura"
    static let accent=UIColor(named: "Accent")
    static let background=UIColor(named: "Background")
    static let tint=UIColor(named: "Tint")
}
